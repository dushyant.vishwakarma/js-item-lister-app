const form = document.getElementById('addForm');
const itemList = document.getElementById('items');
const filter = document.getElementById('filter');

// FORM SUBMIT EVENT
form.addEventListener('submit',(e) => {
    e.preventDefault();

    // GET ITEM'S VALUE
    const inputItem = document.getElementById('item').value;

    // CREATE ITEM LI ELEMENT
    const li = document.createElement('li');
    // ADD CLASS TO THIS LI ELEMENT
    li.className = 'list-group-item';
    // APPEND THIS TEXT NODE TO LI
    li.appendChild(document.createTextNode(inputItem));

    // CREATE DELETE BUTTON
    const deleteBtn = document.createElement('button');
    // ADD CLASSES TO THIS BUTTON
    deleteBtn.className = 'btn btn-danger btn-sm float-right delete';
    // APPEND X (TEXT NODE) TO THIS BUTTON
    deleteBtn.appendChild(document.createTextNode('X'));
    // APPEND DELETE BUTTON TO LI
    li.appendChild(deleteBtn);
    // APPEND LI TO ITEM LIST
    itemList.appendChild(li);
});

// DELETE EVENT
itemList.addEventListener('click',(e) => {
    if(e.target.classList.contains('delete')){
        if(confirm('Are you sure?')){
            const li = e.target.parentElement;
            itemList.removeChild(li);
        }
    }
});

// FILTERING ITEMS FUNCTION
filter.addEventListener('keyup',(e) => {
    // CONVERT TEXT TO LOWERCASE
    const text = e.target.value.toLowerCase();
    // GET LIST 
    const items = document.getElementsByTagName('li');
    // CONVERT TO ARRAY
    Array.from(items).forEach((item) => {
        const itemName = item.firstChild.textContent;

        if(itemName.toLowerCase().indexOf(text) != -1){
            item.style.display = 'block';
        } else {
            item.style.display = 'none';
        }
    });
});

